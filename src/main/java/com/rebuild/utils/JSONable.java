/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONAware;

import java.io.Serializable;


public interface JSONable extends JSONAware, Serializable {

    
    JSON toJSON();

    
    default JSON toJSON(String... specFields) {
        return toJSON();
    }

    @Override
    default String toJSONString() {
        return JSON.toJSONString(toJSON());
    }
}
