/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.notification;

import cn.devezhao.persist4j.engine.ID;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;


public class NotificationOnce {

    private static final ThreadLocal<Set<ID>> STATE = new ThreadLocal<>();

    
    public static void begin() {
        STATE.set(new LinkedHashSet<>());
    }

    
    public static boolean didBegin() {
        return STATE.get() != null;
    }

    
    public static Set<ID> end() {
        Set<ID> set = getMergeSet();
        STATE.remove();
        return Collections.unmodifiableSet(set);
    }

    
    protected static Set<ID> getMergeSet() {
        return STATE.get();
    }
}
