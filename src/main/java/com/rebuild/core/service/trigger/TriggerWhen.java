/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.trigger;

import cn.devezhao.bizz.privileges.impl.BizzPermission;
import com.rebuild.core.privileges.bizz.InternalPermission;


public enum TriggerWhen {

    
    CREATE(BizzPermission.CREATE.getMask()),
    
    DELETE(BizzPermission.DELETE.getMask()),
    
    UPDATE(BizzPermission.UPDATE.getMask()),
    
    ASSIGN(BizzPermission.ASSIGN.getMask()),
    
    SHARE(BizzPermission.SHARE.getMask()),
    
    UNSHARE(InternalPermission.UNSHARE.getMask()),
    
    APPROVED(128),
    
    REVOKED(256),
    
    TIMER(512),

    
    SUBMIT(1024),
    
    REJECTED(2048),

    ;

    private final int maskValue;

    TriggerWhen(int maskValue) {
        this.maskValue = maskValue;
    }

    public int getMaskValue() {
        return maskValue;
    }
}
