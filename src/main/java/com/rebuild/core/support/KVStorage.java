/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support;

import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;
import com.rebuild.core.BootEnvironmentPostProcessor;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.privileges.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;


@Slf4j
public class KVStorage {

    public static final Object SETNULL = new Object();

    private static final String CUSTOM_PREFIX = "custom.";

    
    public static String getCustomValue(String key) {
        return getValue(CUSTOM_PREFIX + key, false, null);
    }

    
    public static void setCustomValue(String key, Object value) {
        setValue(CUSTOM_PREFIX + key, value);
    }

    
    public static void setCustomValue(String key, Object value, boolean throttled) {
        if (throttled) {
            synchronized (THROTTLED_QUEUE_LOCK) {
                THROTTLED_QUEUE.put(key, value);
            }
        } else {
            setCustomValue(key, value);
        }
    }

    
    public static void removeCustomValue(String key) {
        setCustomValue(key, SETNULL);
    }

    

    
    protected static void setValue(final String key, Object value) {
        Object[] exists = Application.createQueryNoFilter(
                "select configId from SystemConfig where item = ?")
                .setParameter(1, key)
                .unique();

        
        if (value == SETNULL) {
            if (exists != null) {
                Application.getCommonsService().delete((ID) exists[0]);
                Application.getCommonsCache().evict(key);
            }
            return;
        }

        Record record;
        if (exists == null) {
            record = EntityHelper.forNew(EntityHelper.SystemConfig, UserService.SYSTEM_USER, false);
            record.setString("item", key);
        } else {
            record = EntityHelper.forUpdate((ID) exists[0], UserService.SYSTEM_USER, false);
        }
        record.setString("value", String.valueOf(value));

        Application.getCommonsService().createOrUpdate(record);
        Application.getCommonsCache().evict(key);
    }

    
    protected static String getValue(final String key, boolean noCache, Object defaultValue) {
        String value = null;

        if (Application.isReady()) {
            
            if (!noCache) {
                value = Application.getCommonsCache().get(key);
                if (value != null) {
                    return value;
                }
            }

            
            Object[] fromDb = Application.createQueryNoFilter(
                    "select value from SystemConfig where item = ?")
                    .setParameter(1, key)
                    .unique();
            value = fromDb == null ? null : StringUtils.defaultIfBlank((String) fromDb[0], null);
        }

        
        if (value == null) {
            value = BootEnvironmentPostProcessor.getProperty(key);
        }

        
        if (value == null && defaultValue != null) {
            value = defaultValue.toString();
        }

        if (Application.isReady()) {
            if (value == null) {
                Application.getCommonsCache().evict(key);
            } else {
                Application.getCommonsCache().put(key, value);
            }
        }

        return value;
    }

    

    private static final Object THROTTLED_QUEUE_LOCK = new Object();
    private static final Map<String, Object> THROTTLED_QUEUE = new ConcurrentHashMap<>();
    private static final Timer THROTTLED_TIMER = new Timer("KVStorage-Timer");

    static {
        final TimerTask localTimerTask = new TimerTask() {
            @Override
            public void run() {
                if (THROTTLED_QUEUE.isEmpty()) return;

                synchronized (THROTTLED_QUEUE_LOCK) {
                    final Map<String, Object> queue = new HashMap<>(THROTTLED_QUEUE);
                    THROTTLED_QUEUE.clear();

                    log.info("Synchronize KV pairs ... {}", queue);
                    for (Map.Entry<String, Object> e : queue.entrySet()) {
                        try {
                            RebuildConfiguration.setCustomValue(e.getKey(), e.getValue());
                        } catch (Throwable ex) {
                            log.error("Synchronize KV error : {}", e, ex);

                            
                            THROTTLED_QUEUE.put(e.getKey(), e.getValue());
                        }
                    }
                }
            }
        };

        THROTTLED_TIMER.scheduleAtFixedRate(localTimerTask, 2000, 2000);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("The KVStorage shutdown hook is enabled");
            localTimerTask.run();
        }));
    }
}
